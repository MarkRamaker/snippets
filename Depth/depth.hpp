/****************************************************************************
 *
 *   Copyright (C) 2019 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain all the copyright
 *    notices, this list of conditions and all the disclaimers.
 * 2. Redistributions in binary form must reproduce all the copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#ifndef __CLEARDEPTH_HPP
#define __CLEARDEPTH_HPP 1

namespace clearDepth {

  class clearDepthProcessor {
    private:
      unsigned int maxPyramids;
      unsigned int maxDisparity;
      int          minDisparity;
      unsigned int minKernelSize;
      unsigned int maxKernelSize;
      unsigned int maxGradient;
      unsigned int fourierKernelSize;
      unsigned int fourierMaxFreq;
      int          basicDisparityOffset;
      unsigned int imageWidth;
      unsigned int imageHeight;
      unsigned int imageWidthStep;
      unsigned char* imageLeftData;
      unsigned char* imageRightData;
      float*       imageDisparity;
      void binPixelImages (unsigned char* from, unsigned char* to, unsigned int height,
                           unsigned int width, unsigned int widthStep);
      void interpolatePixelImages (float* from, float* to,
                                  unsigned int height, unsigned int width);
      void weightedSmoothing (float* from, float* to, unsigned char* ref,
                              float max, unsigned int height, unsigned int width);
      void pyramidRecursion_r (unsigned int height, unsigned int width, unsigned int widthStep,
                               unsigned int recursions, unsigned char* left, unsigned char* right, float* disp);
      void blockBasedDisparityOffset (float sensitivity, unsigned int height, unsigned int width,
                                      unsigned int widthStep, unsigned char* left, unsigned char* right, float* disp);
      unsigned int kernelAbsDiff (unsigned int height, unsigned int width, unsigned int widthStep,
                                  long int leftY, long int leftX, long int rightY, long int rightX,
                                  unsigned char* left, unsigned char* right);

    public:
      int processImages (unsigned char* left, unsigned char* right, float* disp);
      int setMaxPyramids (unsigned int max);
      int setMaxDisparity (unsigned int max);
      int setMinDisparity (int min);
      int setMinKernelSize (unsigned int min);
      int setMaxKernelSize (unsigned int max);
      int setMaxGradient (unsigned int max);
      int setFourierKernelSize (unsigned int size);
      int setFourierMaxFreq (unsigned int max);
      int setBasicDisparityOffset (unsigned int offset);
      clearDepthProcessor (unsigned int height, unsigned int width, unsigned int widthStep); // Constructor.
      virtual ~clearDepthProcessor (void); // Destructor.
  };

}

#endif /* end of include guard:  */
