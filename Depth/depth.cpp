/****************************************************************************
 *
 *   Copyright (C) 2019 Mark Ramaker. All rights reserved.
 *   Author: Mark Ramaker <markramaker@outlook.com>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, is prohibited unless written consent from the copyright holder
 * is obtained.
 *
 * When written permission is obtained the following conditions apply:
 *
 * 1. Redistributions of source code must retain all the copyright
 *    notices, this list of conditions and all the disclaimers.
 * 2. Redistributions in binary form must reproduce all the copyright
 *    notices, this list of conditions and all disclaimers in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS
 * OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 ****************************************************************************/
#include <depth.hpp>

#include <iostream>
#include <opencv2/highgui.hpp>
#include <cv.h>
#include <highgui.h>

namespace clearDepth {
  IplImage * debug;

    /**
     * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
     *
     *
     * @param height (I) the input height for the processor.
     * @param width (I) the input width for the processor.
     * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
     */
    inline unsigned int safeIndex (long int index, unsigned int max) {
      if (index < 0) return 0;
      return index < max ? index : max - 1;
    }


    /**
     * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
     *
     *
     * @param height (I) the input height for the processor.
     * @param width (I) the input width for the processor.
     * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
     */
    inline unsigned int absDiff (unsigned int x, unsigned int y) {
      return x > y ? x - y : y - x;
    }

  /**
   * @brief clearDepthProcessor object constructor.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  clearDepthProcessor::clearDepthProcessor (unsigned int height, unsigned int width, unsigned int widthStep) {
    // Init the image params.
    this->imageHeight = height;
    this->imageWidth = width;
    this->imageWidthStep = widthStep;
    // Init the other variables to sane defaults.
    this->maxPyramids = 5;
    this->maxDisparity = 5;
    this->minDisparity = -(this->maxDisparity);// / 2);
    this->minKernelSize = 4;
    this->maxKernelSize = this->minKernelSize + this->maxDisparity * 30;//4;
    this->maxGradient = 1000;
    this->fourierKernelSize = this->minKernelSize * 2;
    this->fourierMaxFreq = this->fourierKernelSize / 4;
    this->basicDisparityOffset = 1;
  }


  /**
   * @brief clearDepthProcessor object constructor.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  clearDepthProcessor::~clearDepthProcessor (void) {

  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  int clearDepthProcessor::processImages (unsigned char* left, unsigned char* right, float* disp) {
    float dispBuff [this->imageHeight][this->imageWidth];
    // Run course to fine. disp will be initialized after this!
    std::cout << "width: " << this->imageWidth << " widthStep: " << this->imageWidthStep << "\n";
    this->pyramidRecursion_r(this->imageHeight, this->imageWidth, this->imageWidthStep, 0, left, right, (float*)dispBuff);

    // Top level block mathing. FIXME make kernels smaller using first param?
    this->blockBasedDisparityOffset (1, this->imageHeight, this->imageWidth, this->imageWidthStep, left, right, (float*)dispBuff);
    this->weightedSmoothing ((float*)dispBuff, disp, left, 128, this->imageHeight, this->imageWidth);
    // FIXME implement smoothness constraint? FIXME implement fourier subpixel calculation (gausian window...)
    // FIXME implement disp to depth?
    // FIXME track min, max values

    return 0;

  }

  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  void clearDepthProcessor::binPixelImages (unsigned char* from, unsigned char* to, unsigned int height, unsigned int width, unsigned int widthStep) {
      unsigned char (*fromPtr)  [height][widthStep] = (unsigned char (*) [height][widthStep]) from;
      unsigned char (*toPtr) [height / 2][width /2 ] = (unsigned char (*) [height / 2][width / 2]) to;
      for (unsigned int yi = 0; yi < height - 1; yi += 2) for (unsigned int xi = 0; xi < width - 1; xi += 2) {
        unsigned int t = 0;
        // Bin 4 pixels into leftBin.
        t += (*fromPtr)[yi + 0][xi + 0];
        t += (*fromPtr)[yi + 1][xi + 0];
        t += (*fromPtr)[yi + 0][xi + 1];
        t += (*fromPtr)[yi + 1][xi + 1];
        (*toPtr) [yi / 2][xi / 2] = t / 4;
      }
    }

    /**
     * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
     *
     *
     * @param height (I) the input height for the processor.
     * @param width (I) the input width for the processor.
     * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
     */
    void clearDepthProcessor::interpolatePixelImages (float* from, float* to, unsigned int height, unsigned int width) {
      float (*toPtr)  [height][width] = (float (*) [height][width]) to;
      float (*fromPtr) [height / 2][width /2 ] = (float (*) [height / 2][width / 2]) from;
      for (unsigned int yi = 0; yi < height / 2; yi++) for (unsigned int xi = 0; xi < (width - 1) / 2; xi++) {
        (*toPtr) [yi * 2 + 0][xi * 2 + 0] = (*fromPtr) [yi + 0][xi + 0] * 2;
        (*toPtr) [yi * 2 + 0][xi * 2 + 2] = (*fromPtr) [yi + 0][xi + 1] * 2;
        (*toPtr) [yi * 2 + 0][xi * 2 + 1] = ((*toPtr) [yi * 2 + 0][xi * 2 + 0] + (*toPtr) [yi * 2 + 0][xi * 2 + 2]) / 2;
      }
      if (height % 2 == 0) {
        for (unsigned int yi = 0; yi < height / 2; yi++) {
          (*toPtr) [yi * 2][width -1] = (*toPtr) [yi * 2][width - 2] * 2 - (*toPtr) [yi * 2][width - 3];
        }
      }
      for (unsigned int yi = 0; yi < (height - 1) / 2; yi++) for (unsigned int xi = 0; xi < width; xi++) {
        (*toPtr) [yi * 2 + 1][xi] = ((*toPtr) [yi * 2 + 0][xi] + (*toPtr) [yi * 2 + 2][xi]) / 2;
      }
      if (height % 2 == 0) {
        for (unsigned int xi = 0; xi < width; xi++) {
          (*toPtr) [height - 1][xi] = (*toPtr) [height - 2][xi] * 2 - (*toPtr) [height - 3][xi];
        }
      }
      if (height % 2 == 0 && width % 2 == 0) {
        (*toPtr) [height - 1][width - 1] = (*toPtr) [height - 2][width - 2];
      }
    }

    /**
     * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
     *
     *
     * @param height (I) the input height for the processor.
     * @param width (I) the input width for the processor.
     * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
     */
    void clearDepthProcessor::weightedSmoothing (float* from, float* to, unsigned char* ref, float max, unsigned int height, unsigned int width) {
      float (*toPtr)  [height][width] = (float (*) [height][width]) to;
      float (*fromPtr) [height][width] = (float (*) [height][width]) from;
      unsigned char (*refPtr) [height][width] = (unsigned char (*) [height][width]) ref;

      float f, a, b;
      unsigned int c;
      int ksize = 1;

      for (unsigned int yi = 0; yi < height; yi++) for (unsigned int xi = 0; xi < width; xi++) {
        c = 0;
        b = 0;
        for (int yyi = -ksize; yyi <= ksize; yyi++) for (int xxi = -ksize; xxi <= ksize; xxi++) {
          c += (*refPtr)[safeIndex(yi + yyi, height)][safeIndex(xi + xxi, width)];
        }
        c /= (ksize * 2 + 1) * (ksize * 2 + 1);
        f = absDiff(c, (*refPtr)[yi][xi]) / max;
        if (f > 1) f = 1;
        for (int yyi = -ksize; yyi <= ksize; yyi++) for (int xxi = -ksize; xxi <= ksize; xxi++) {
          b += (*fromPtr)[safeIndex(yi + yyi, height)][safeIndex(xi + xxi, width)];
        }
        a = (*fromPtr)[yi][xi];
        b -= a;
        b /= (ksize * 2 + 1) * (ksize * 2 + 1) - 1;
        //if (f > 0.5) std::cout << "f, " << f << " a " << a << " b " << b << "\n";
        (*toPtr)[yi][xi] = f * a + (1 - f) * b;
      }


    }

  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  void clearDepthProcessor::pyramidRecursion_r (unsigned int height, unsigned int width, unsigned int widthStep, unsigned int recursions, unsigned char* left, unsigned char* right, float* disp) {
    unsigned char (*leftPtr)  [height][widthStep] = (unsigned char (*) [height][widthStep]) left;
    unsigned char (*rightPtr) [height][widthStep] = (unsigned char (*) [height][widthStep]) right;
    float (*dispPtr) [height][widthStep] = (float (*) [height][widthStep]) disp;
    unsigned char leftBin [height / 2][width / 2];
    unsigned char rightBin [height / 2][width / 2];
    float dispBin [height / 2][width / 2];
    float dispPreFilter [height][width];

    // Basic case of no recursion.
    if (this->maxPyramids == 0) {
      // End of recursions we are the last one. Fill disp with start offsets.
      for (unsigned int yi = 0; yi < height; yi++) for (unsigned int xi = 0; xi < width; xi++) (*dispPtr) [yi][xi] = this->basicDisparityOffset;
      return;
    }

    // Copy and bin into the individual storage arrays, disp will get filled recursively so leave it empty.
    this->binPixelImages(left, (unsigned char*)leftBin, height, width, widthStep);
    this->binPixelImages(right, (unsigned char*)rightBin, height, width, widthStep);

    // Recursion and halt condition here, half the pixel resolution.
    if (this->maxPyramids > recursions + 1) {
      this->pyramidRecursion_r (height / 2, width / 2, width / 2, recursions + 1, (unsigned char*)leftBin, (unsigned char*)rightBin, (float*)dispBin);
    } else {
      // End of recursions we are the last one. Fill disp with start offsets.
      for (unsigned int yi = 0; yi < height / 2; yi++) for (unsigned int xi = 0; xi < width / 2; xi++) dispBin [yi][xi] = this->basicDisparityOffset;
    }

    // Returning from recursion do depth calculation on this level.
    this->blockBasedDisparityOffset (1 /*/ (recursions * recursions + 1)*/, height / 2, width / 2, width / 2, (unsigned char*)leftBin, (unsigned char*)rightBin, (float*)dispBin);

    // Copy the disparity to the disparity map in the level above, and multiply by 2 for the increase in resolution.
    this->interpolatePixelImages ((float*) dispBin, (float*)dispPreFilter, height, width);

    // Filter disp.
    this->weightedSmoothing ((float*)dispPreFilter, disp, left, 128, height, width);

    #if 1
      float max = (*dispPtr)[0][0];
      float min = (*dispPtr)[0][0];
      float minClamp = -100000;//10 / (recursions + 1);
      float maxClamp = 100000;// / (recursions + 1);
      if (min < minClamp) min = minClamp;
      if (max > maxClamp) max = maxClamp;
      for (int yi = 0; yi < height; yi++) for (int xi = 0; xi < width; xi++) {
        if ((*dispPtr)[yi][xi] < minClamp) {
          // std::cout << "yi " << yi << " xi " << xi << " val " << buff[yi][xi] << "\n ";
          (*dispPtr)[yi][xi] = minClamp;
        }
        if ((*dispPtr)[yi][xi] > maxClamp) {
          // std::cout << "yi " << yi << " xi " << xi << " val " << buff[yi][xi] << "\n ";
          (*dispPtr)[yi][xi] = maxClamp;
        }
        if ((*dispPtr)[yi][xi] > max) {
          max = (*dispPtr)[yi][xi];
        }
        if ((*dispPtr)[yi][xi] < min) {
          min = (*dispPtr)[yi][xi];
        }
      }
      min = (0) - min;
      max += min;
      max = 255 / max;
       debug = cvCreateImage(cvSize(width, height), IPL_DEPTH_8U, 1);
       for (unsigned int yi = 0; yi < height; yi++) for (unsigned int xi = 0; xi < width; xi++) {
         debug->imageData [yi * debug->widthStep + xi] = ((*dispPtr) [yi][xi] + min) * max;
       }
       cvShowImage("result", debug);
       cvWaitKey(0);
       cvReleaseImage(&debug);
    #endif

    // Done with this pyramid level, return.
    return;
  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  class kernelObject {
    private:
      unsigned int kernelHeight;
      unsigned int kernelWidth;
      unsigned int imageHeight;
      unsigned int imageWidth;
      unsigned int imageWidthStep;
    public:
      void calibrateLength (unsigned char* reference, unsigned int xOffset, unsigned int yOffset, unsigned int maxGradient, unsigned int maxLength);
      unsigned long int runKernel (unsigned char* left, unsigned char* right, unsigned int xLeftOffset, long int xRightOffset, unsigned int yOffset);
      unsigned int getKernelLength (void);
      kernelObject (unsigned int imageHeight, unsigned int imageWidth, unsigned int imageWidthStep, unsigned int minSize);
      virtual ~kernelObject (void);
  };


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  kernelObject::kernelObject (unsigned int imageHeight, unsigned int imageWidth, unsigned int imageWidthStep, unsigned int minSize) {
    this->kernelHeight = minSize;
    this->kernelWidth = minSize;
    this->imageHeight = imageHeight;
    this->imageWidth = imageWidth;
    this->imageWidthStep = imageWidthStep;
  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  kernelObject::~kernelObject () {

  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  void kernelObject::calibrateLength (unsigned char* reference, unsigned int xOffset, unsigned int yOffset, unsigned int maxGradient, unsigned int maxLength) {
    unsigned char (*refPtr) [this->imageHeight][this->imageWidthStep] = (unsigned char (*) [this->imageHeight][this->imageWidthStep]) reference;
    unsigned long int gradientCollector = 0;
    long int meanCollector [this->kernelHeight];
    long int xi, yi;
    unsigned int xxi, yyi;

    for (unsigned int xi = 0; xi < this->kernelHeight; xi++) meanCollector [xi] = 0;

    // Keep adding Collumns until either the end of the image, the max kernel length or enough entropy has been gathered.
    for (xxi = 0, xi = xOffset - this->kernelHeight / 2; xxi < maxLength && gradientCollector < maxGradient && xi < this->imageWidth; xxi++, xi++) {
      // Itterate over the rows in this Collumn offset.
      for (yyi = 0, yi = yOffset - this->kernelHeight / 2; yyi < this->kernelHeight; yyi++, yi++) {
        // Accumulate the entropy.
        meanCollector [yyi] += (*refPtr) [safeIndex(yi, this->imageHeight)][safeIndex(xi, this->imageWidth)];
        long int t = absDiff(meanCollector [yyi] / (xxi + 1), (*refPtr) [safeIndex(yi, this->imageHeight)][safeIndex(xi + 1, this->imageWidth)]);
        gradientCollector += t * t;
      }
    }
    // The kernel can never be smaller then Square.
    if (xxi > this->kernelHeight) {
      // Elongated kernel.
      this->kernelWidth = xxi;
    } else {
      // Square kernel.
      this->kernelWidth = this->kernelHeight;
    }
  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  unsigned long int kernelObject::runKernel (unsigned char* left, unsigned char* right, unsigned int xLeftOffset, long int xRightOffset, unsigned int yOffset) {
    unsigned char (*leftPtr)  [this->imageHeight][this->imageWidthStep] = (unsigned char (*) [this->imageHeight][this->imageWidthStep]) left;
    unsigned char (*rightPtr) [this->imageHeight][this->imageWidthStep] = (unsigned char (*) [this->imageHeight][this->imageWidthStep]) right;
    unsigned long int sadCollector = 0;
    long int xil, xir, yi;
    unsigned int xxi, yyi;

    // For every row.
    for (yyi = 0, yi = yOffset - this->kernelHeight / 2; yyi <= this->kernelHeight; yyi++, yi++) {
      // For every column.
      for (xxi = 0, xil = xLeftOffset - this->kernelHeight / 2, xir = xRightOffset - this->kernelHeight / 2; xxi <= this->kernelWidth; xxi++, xil++, xir++) {
        // Accumulate the absolute difference.
        sadCollector += absDiff((*leftPtr) [safeIndex(yi, this->imageHeight)][safeIndex(xil, this->imageWidth)], (*rightPtr) [safeIndex(yi, this->imageHeight)][safeIndex(xir, this->imageWidth)]);
      }
    }
    return sadCollector;
  }


  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  unsigned int kernelObject::getKernelLength (void) {
    return this->kernelWidth - this->kernelHeight + 1;
  }



  /**
   * @brief pyramidRecursion_r runs the pyramid itterative block mathing.
   *
   *
   * @param height (I) the input height for the processor.
   * @param width (I) the input width for the processor.
   * @param widthStep (I) the number of chars in each row, this is to be able to work with alligned image rows.
   */
  void clearDepthProcessor::blockBasedDisparityOffset (float sensitivity, unsigned int height, unsigned int width, unsigned int widthStep, unsigned char* left, unsigned char* right, float* disp) {
    float (*dispPtr) [height][widthStep] = (float (*) [height][widthStep]) disp;
    kernelObject kernel (height, width, widthStep, this->minKernelSize);

    // For every pixel.
    for (long int yi = 0; yi < height; yi++) for (long int xi = 0; xi < width; xi += kernel.getKernelLength ()) {
      // Calibrate the kernel size.
      kernel.calibrateLength (left, xi, yi, this->maxGradient * sensitivity, this->maxKernelSize * sensitivity);

      // Get the average previous calculated depth for this kernel size.
      long int prevOffset = 0; // Init at 0.
      for (unsigned int di = 0; di < kernel.getKernelLength (); di++) prevOffset += (*dispPtr)[yi][xi + di]; // Accumulate.
      prevOffset /= kernel.getKernelLength (); // Average.

      long int bestIndex = this->minDisparity; // Best search window offset tracker.
      unsigned long int bestSad = 0xFFFFFFFF; // Best SAD tracker, init at max value (smaller is better).
      unsigned long int tempSad; // Return value container.

      // Loop over the search window.
      for (long int ki = bestIndex; ki < this->maxDisparity; ki++) {
        // Run the kernel and save the resulting SAD.
        tempSad = kernel.runKernel (left, right, xi, xi + ki + prevOffset, yi);

        // Compare the previous best to the new result.
        if (tempSad < bestSad) {
          bestSad = tempSad;
          bestIndex = ki;
        }
      }

      // Update the search window best result with the previous offset (offset of the search window).
      bestIndex += prevOffset;
      // Loop through and set the disparity values.
      for (unsigned int di = 0; di < kernel.getKernelLength (); di++) (*dispPtr)[yi][xi + di] = bestIndex;
    }
  }
}










#if 0
  float max = dispBin[0][0];
  float min = dispBin[0][0];
  float minClamp = 0;//10 / (recursions + 1);
  float maxClamp = 70 / (recursions + 1);
  if (min < minClamp) min = minClamp;
  if (max > maxClamp) max = maxClamp;
  for (int yi = 0; yi < height / 2; yi++) for (int xi = 0; xi < width / 2; xi++) {
    if (dispBin[yi][xi] < minClamp) {
      // std::cout << "yi " << yi << " xi " << xi << " val " << buff[yi][xi] << "\n ";
      dispBin[yi][xi] = minClamp;
    }
    if (dispBin[yi][xi] > maxClamp) {
      // std::cout << "yi " << yi << " xi " << xi << " val " << buff[yi][xi] << "\n ";
      dispBin[yi][xi] = maxClamp;
    }
    if (dispBin[yi][xi] > max) max = dispBin[yi][xi];
    if (dispBin[yi][xi] < min) min = dispBin[yi][xi];
  }
  std::cout << "min, " << min << " max, " << max << "\n";
  min = (0) - min;
  max += min;
  max = 255 / max;
   debug = cvCreateImage(cvSize(width / 2, height / 2), IPL_DEPTH_8U, 1);
   for (unsigned int yi = 0; yi < height / 2; yi++) for (unsigned int xi = 0; xi < width / 2; xi++) {
     debug->imageData [yi * debug->widthStep + xi] = (dispBin [yi][xi] + min) * max;
   }
   cvShowImage("result", debug);
   cvWaitKey(0);
   cvReleaseImage(&debug);
#endif







#if 0

for (unsigned int yi = 0; yi < height; yi++) for (unsigned int xi = 0; xi < width; xi++) {
  (*dispPtr) [yi + 0][xi + 0] = dispBin [safeIndex(yi / 2, height / 2)][safeIndex(xi / 2, width / 2)] * 2;
}

for (unsigned int yi = 0; yi < height / 2; yi++) for (unsigned int xi = 0; xi < (width - 1) / 2; xi++) {
  (*dispPtr) [yi * 2 + 0][xi * 2 + 0] = dispBin [yi + 0][xi + 0] * 2;
  (*dispPtr) [yi * 2 + 0][xi * 2 + 2] = dispBin [yi + 0][xi + 1] * 2;
  (*dispPtr) [yi * 2 + 0][xi * 2 + 1] = ((*dispPtr) [yi * 2 + 0][xi * 2 + 0] + (*dispPtr) [yi * 2 + 0][xi * 2 + 2]) / 2;
}
if (height % 2 == 0) {
  for (unsigned int yi = 0; yi < height / 2; yi++) {
    (*dispPtr) [yi * 2][width -1] = (*dispPtr) [yi * 2][width - 2] * 2 - (*dispPtr) [yi * 2][width - 3];
  }
}
for (unsigned int yi = 0; yi < (height - 1) / 2; yi++) for (unsigned int xi = 0; xi < width; xi++) {
  (*dispPtr) [yi * 2 + 1][xi] = ((*dispPtr) [yi * 2 + 0][xi] + (*dispPtr) [yi * 2 + 2][xi]) / 2;
}
if (height % 2 == 0) {
  for (unsigned int xi = 0; xi < width; xi++) {
    (*dispPtr) [height - 1][xi] = (*dispPtr) [height - 2][xi] * 2 - (*dispPtr) [height - 3][xi];
  }
}
#endif



    #ifdef DEBUG
       debug = cvCreateImage(cvSize(width / 2, height / 2), IPL_DEPTH_8U, 1);
       for (unsigned int yi = 0; yi < height / 2; yi++) for (unsigned int xi = 0; xi < width / 2; xi++) {
         debug->imageData [yi * debug->widthStep + xi] = leftBin [yi][xi];
       }
       cvShowImage("result", debug);
       cvWaitKey(0);
       cvReleaseImage(&debug);
    }
    #endif
