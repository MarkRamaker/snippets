#include "heap.h"
#include <string.h>

/*!
 * \struct
 * */
struct heap {
	size_t size; /**< The amount of data elements the heap able to contain. */
	size_t data_width; /**< The data width of the data elements. */
	size_t count; /**< A count of how many data elements are stored. */
	BOOL (higher_priority)(void*, void*); /**< The higher_priority function return a true value is arg1 has to be higher in the heap than arg2. */
	void* heap; /**< Pointer to the storage array. */
};

/*!
 * \fn
 * \brief This swaps two regions of memory.
 * \param [in,out] i1 Memory region one.
 * \param [in,out] i2 Memory region two.
 * \param [in] width The width of the data to be swapped.
 * \return void.
 * */
void mem_swap (char   *i1,
               char   *i2,
               size_t  width) {
	for (; width; width--) {
		i1 [width] ^= i2 [width];
		i2 [width] ^= i1 [width];
		i1 [width] ^= i2 [width];
	}
	i1 [width] ^= i2 [width];
	i2 [width] ^= i1 [width];
	i1 [width] ^= i2 [width];
}

/*!
 * \fn
 * \brief This function calculates the parent location based on current location.
 * \param [in] base The base location of the heap container.
 * \param [in] index The index from where the parent must be located.
 * \param [in] width The width of the data elements in the heap.
 * \return returns the location of the parent in a char* format.
 * */
char* parent_index (char   *base,
                    char   *index,
                    size_t  width) {
	return base + ((index - base - width) >> 1);
}

/*!
 * \fn
 * \brief This function calculates the left child location based on the current location.
 * \param [in] base The base location of the heap container.
 * \param [in] index The index from where the left child must be located.
 * \param [in] width The width of the data elements in the heap.
 * \return returns the location of the left child in a char* format.
 * */
char* left_child_index (char   *base,
			char   *index,
			size_t  width) {
	return base + ((index - base) << 1) + width);
}

/*!
 * \fn
 * \brief This function calculates the right child location based on the current location.
 * \param [in] base The base location of the heap container.
 * \param [in] index The index from where the right child must be located.
 * \param [in] width The width of the data elements in the heap.
 * \return returns the location of the right child in a char* format.
 * */
char* right_child_index (char   *base,
			 char   *index,
			 size_t  width) {
	return base + ((index - base) << 1) + (width << 1));
}

/*!
 * \fn
 * \brief This function sifts down an element recursively until it reaches it's priority location, starting from index.
 * \param [in] heap The heap instance that the operation needs to be applied on..
 * \param [in] index The index from where the sifting needs to start.
 * \return void.
 * */
void sift_down_r (heap_t *heap,
                  char   *index) {
	// Recursion stop condition.
	if ((index - heap->heap << 1) + (heap->data_width << 1) > heap->count * heap->data_width) {
		// Test to see if there is no other child.
		if ((index - heap->heap << 1) + heap->data_width > heap->count * heap->data_width) return;

		// Last child sift test.
		if (heap->higher_priority (index, left_child_index (heap, index)) mem_swap (index, left_child_index (heap, index), heap->data_width);
	}
	// Which side to sift to.
	if (heap->higher_priority (index, left_child_index (heap, index)) {
		mem_swap (index, left_child_index (heap, index), heap->data_width);
	} else if (heap->higher_priority (index, right_child_index (heap, index)) {
		mem_swap (index, right_child_index (heap, index), heap->data_width);
	}
}

/*!
 * \fn
 * \brief This function pulls down elements recursively untill it reaches the top, starting from index.
 * \param [in] heap The heap instance that the operation needs to be applied on..
 * \param [in] index The index from where the sifting needs to start.
 * \return void.
 * */
void pull_down_r (heap_t *heap,
                 char   *index) {
	// Recursion stop condition.
	if (index == heap->heap) return;

	// Pull parent down to our level.
	memcpy (index, parent_index (heap, index), heap->data_width);

	// Recursion on the parent node.
	pull_down_r (heap, parent_index (heap, index);
}

/*!
 * \fn
 * \brief This function initializes a heap instance.
 * \param [in] heap The heap instance to be initialized.
 * \param [in] size The amount of data elements the heap must be able to contain.
 * \param [in] data_width The width of the data elements which the heap must be able to contain.
 * \param [in] higher_priority This function is used to assert which element has to be higher in the heap.
 * \return Returns a heap_t instance initialized as well.
 * */
heap_t* heap_init (heap_t* heap,
		   size_t size,
		   size_t data_width,
		   BOOL (higher_priority)(void *, void *)) {
	heap->size = size;
	heap->count = 0;
	heap->data_width = data_width;
	heap->higher_priority = higher_priority;
	heap->heap = (char*) (r + 1);
}

/*!
 * \fn
 * \brief This function creates a heap instance, from dynamic memory and initializes it.
 * \param [in] size The amount of data elements the heap must be able to contain.
 * \param [in] data_width The width of the data elements which the heap must be able to contain.
 * \param [in] higher_priority This function is used to assert which element has to be higher in the heap.
 * \return Returns a heap_t instance initialized as well.
 * */
heap_t* heap_create (size_t size,
		     size_t data_width,
		     BOOL (higher_priority)(void *, void *)) {
	heap_t* r = (heap_t*) malloc (sizeof (heap_t) + size * data_width);
	if (r) {
		heap_init (r, size, data_width, higher_priority);
	}
	return r;
}

/*!
 * \fn
 * \brief This function copies out the top element of the heap, it however is not removed.
 * \param [in] heap The heap instance to be used.
 * \param [out] value A pointer to the location the element must be copied to.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_find (heap_t* heap, void* value) {
	if (heap->count) memcpy (value, heap->heap, heap->data_width);
	else return FALSE;
	return TRUE;
}

/*!
 * \fn
 * \brief This function copies out the top element of the heap, it is also removed from the heap.
 * \param [in] heap The heap instance to be used.
 * \param [out] value A pointer to the location the element must be copied to.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_pop (heap_t* heap, void* value) {
	char* hheap = (char*) heap->heap;
	if (heap->count) {
		// Start by doing the return copy of the element.
		memcpy (value, hheap, heap->data_width);

		// Move up the lowest element of the heap.
		memcpy (hheap, hheap + heap->count * heap->data_width, heap->data_width);

		// Effectivele remove one element by updating heap information.
		heap->count--;

		// Sift down the top heap element.
		sift_down_r (heap, hheap);

		// Success.
		return TRUE;

	} else return FALSE;
}

/*!
 * \fn
 * \brief This function adds an element to the heap instance.
 * \param [in] heap The heap instance to be used.
 * \param [in] value A pointer to the the element to be used.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_push (heap_t* heap, void* value) {
	char* hheap = (char*) heap->heap;
	if (heap->size - heap->count > 0) {
		// Start by pulling elements successive down towards the bottom empty location.
		pull_down_r (heap, hheap + heap->count * heap->data_width);

		// Insert the new element in the top of the heap.
		memcpy (hheap, value, heap->data_width);

		// Sift down the element to it's desired location.
		sift_down_r (heap, hheap);

		// Update heap information.
		heap->count++;

		// Success
		return TRUE;

	} else return FALSE;
}
