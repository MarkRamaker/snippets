#ifndef HEAP_IMPLEMENT_H
#define HEAP_IMPLEMENT_H 1
#include <stdlib.h>

#ifndef BOOL
#define BOOL char
#define false 0
#define FALSE false
#define true !false
#define TRUE true
#endif

/*!
 * \typedef
 * \brief A forward declaration of the heap type.
 * */
typedef struct heap heap_t;

/*!
 * \fn
 * \brief This function initializes a heap instance.
 * \param [in] heap The heap instance to be initialized.
 * \param [in] size The amount of data elements the heap must be able to contain.
 * \param [in] data_width The width of the data elements which the heap must be able to contain.
 * \param [in] higher_priority This function is used to assert which element has to be higher in the heap.
 * \return Returns a heap_t instance initialized as well.
 * */
heap_t* heap_init (heap_t* heap,
		   size_t size,
		   size_t data_width,
		   BOOL (higher_priority)(void *, void *));

/*!
 * \fn
 * \brief This function creates a heap instance, from dynamic memory and initializes it.
 * \param [in] size The amount of data elements the heap must be able to contain.
 * \param [in] data_width The width of the data elements which the heap must be able to contain.
 * \param [in] higher_priority This function is used to assert which element has to be higher in the heap.
 * \return Returns a heap_t instance initialized as well.
 * */
heap_t* heap_create (size_t size,
		     size_t data_width,
		     BOOL (higher_priority)(void *, void *));

/*!
 * \fn
 * \brief This function copies out the top element of the heap, it however is not removed.
 * \param [in] heap The heap instance to be used.
 * \param [out] value A pointer to the location the element must be copied to.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_find (heap_t* heap, void* value);

/*!
 * \fn
 * \brief This function copies out the top element of the heap, it is also removed from the heap.
 * \param [in] heap The heap instance to be used.
 * \param [out] value A pointer to the location the element must be copied to.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_pop (heap_t* heap, void* value);

/*!
 * \fn
 * \brief This function adds an element to the heap instance.
 * \param [in] heap The heap instance to be used.
 * \param [in] value A pointer to the the element to be used.
 * \return BOOL A success, failure boolean.
 * */
BOOL heap_push (heap_t* heap, void* value);
#endif
